# Changelog

This file is inspired from [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), this project
follows the [Semantic Versioning](https://semver.org/). Here are the evolution types:
- `Added` for new features.
- `Changed` for changes in existing functionality.
- `Deprecated` for soon-to-be removed features.
- `Removed` for now removed features.
- `Fixed` for any bug fixes.
- `Security` in case of vulnerabilities.

## Unreleased

## v0.3.4 (2021-04-19)

### Fixed

- Fix the consensus computation for different segment sizes. (#24)
- The `FAMSA` workdir must be platform agnostic. (#25)

## v0.3.3 (2021-03-02)

### Modified

- The pandas concatenation are optimized in `compute_depth_match` and `compute_markers`. (#22)
- The FAMSA logs are deactivated.

## v0.3.2 (2021-02-08)

### Added

- Unit test for marker computation function. (#12)

### Fixed

- Marker computation is fixed when all markers are located at the same depth (indexing bug). (#21)

## v0.3.1 (2021-01-06)

### Fixed

- Group indexing is done through `.iloc` instead of `.loc` during marker computation. (#20)

## v0.3.0 (2021-01-05)

### Modified

- Bin segmentation is now done through BINSEG algorithm, instead of PELT. (#16)
- Marker computation is extracted from `WellCorrelation` scope. (#19)
- The `WellCorrelation` class attributes are renamed as protected attributes, following Python
  conventions.

### Fixed

- Consensus building is fixed for `min_seg>1`. (#16)
- Fix PELT method parameter order. (#17)
- Tests are updated (there is a new unit test, and the functional test is simplified).

## v0.2.2 (2020-12-21)

### Modified

- Slight change of a WellCorrelation method name: `extract_top_indices` becomes
  `extract_consensus_indices`.
- Define the marker index in the consensus as a new class attribute.
- Modify the WellCorrelation attribute names so as to consider most of them as protected ones.

## v0.2.1 (2020-12-16)

### Modified

- `FAMSA` is stored in `$(HOME)/.local/share`, a symbolic link is drawn from
  `$(HOME)/.local/bin`. (#14)
- Working `.fasta` files are moved to `/tmp`.

### Fixed

- Version variable name in `__init__.py` is now `__version__`.

## v0.2.0 (2020-12-02)

### Added

- Design some unit tests for data preparation and a functional test in order to target legacy
  results.
- Design a class method aiming at handling manually-defined depths.
- Commit an example of resistivity dataset.
- Enrich docstrings.

### Changed

- Update the `famsa` in/out file paths.
- Rename the main class as `WellCorrelation`.
- Update the class attributes and corresponding `__init__` method parameters.
- Reorganize the perimeter of `WellCorrelation` class methods.
- Refactor the demonstration notebook.

## v0.1.0 (2020-11-05)

### Added

- Compute well correlation structure from an initial research POC.
- Init some example in Jupyter notebooks
- Patch the `FAMSA` program
- Package the project
